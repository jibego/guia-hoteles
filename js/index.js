$(function() {

  $("[data-toggle='tooltip']").tooltip();
  $('.carousel').carousel({
    interval: 2000
  });

  $('#contacto').on('show.bs.modal', function (e) {
    console.log('el modal contacto se está mostrando');
    $('#contactoBtn-sp').removeClass('btn-outline-info');
    $('#contactoBtn-sp').addClass('btn-primary');
    $('#contactoBtn-sp').prop('disabled',true);
  });
  $('#contacto').on('shown.bs.modal', function (e) {
    console.log('el modal contacto se mostró');
  });
  $('#contacto').on('hide.bs.modal', function (e) {
    console.log('el modal contacto se oculta');
  });
  $('#contacto').on('hidden.bs.modal', function (e) {
    console.log('el modal contacto se ocultó');
    $('#contactoBtn-sp').prop('disabled',false);
    $('#contactoBtn-sp').removeClass('btn-primary');
    $('#contactoBtn-sp').addClass('btn-outline-info');
  });

});

$(function() {
  $("[data-toggle='popover']").popover();
});
